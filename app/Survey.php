<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Question;

class Survey extends Model
{
    protected $fillable = [
        'title',
        'detail',
        'publishedAt',
    ];

    public function articles()
    {
        return $this->belongsToMany('App\Question');
    }
}
