<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Survey;
use App\User;

class Question extends Model
{
    protected $fillable = [
        'question',
        'questionType',
        'slug'
    ];

    /**
     * Get the categories associated with the given article.
     *
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function surveys()
    {
        return $this->belongsToMany('App\Survey');
    }

    /**
     * Get the user associated with the given article
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
