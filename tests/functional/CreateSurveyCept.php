<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new survey');

// log in as your admin user
// This should be id of 1 if you created your manual login for a known user first.
Auth::loginUsingId(1);

// When
$I->amOnPage('/admin/surveys');
$I->see('Survey', 'h1');
$I->dontSee('Randomtest');
// And
$I->click('Add Survey');

// Then
$I->amOnPage('/admin/surveys/create');
// And
$I->see('Add Survey', 'h1');
$I->submitForm('.createsurvey', [
    'name' => 'Randomtest',
]);
// Then
$I->seeCurrentUrlEquals('/admin/surveys');
$I->see('Survey', 'h1');
$I->see('New survey added!');
$I->see('Randomtest');