<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('delete a question');

// log in as your admin user
// This should be id of 1 if you created your manual login for a known user first.
Auth::loginUsingId(1);

// Add db test data

// add a test user
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'testuser1',
    'email' => 'test1@user.com',
    'password' => 'password',
]);

// Add test category
$I->haveRecord('surveys', [
    'id' => '9900',
    'title' => 'survey1',
    'detail' => 'survey1 detail',
]);



// add a test article to delete
$I->haveRecord('questions', [
    'id' => '9000',
    'question' => 'Question 1',
    'questionType' => 'text, required',
    'slug' => 'question1',
    'author_id' => '9999',
]);


// add link data for article and category for the test entry
$I->haveRecord('survey_questions', [
    'article_id' => '9000',
    'category_id' => '9900',
]);


// Check the user is in the db and can be seen
$I->seeRecord('questions', ['question' => 'Question1', 'id' => '9000']);


// When
$I->amOnPage('/admin/questions');

// then

// Check  the link is present - this is because there could potentially be many update links/buttons.
// each link can be identified by the users id as name.
$I->seeElement('a', ['name' => '9000']);
// And
$I->click('Delete Questions1');

// Then
$I->amOnPage('/admin/questions');
// And
$I->dontSeeElement('a', ['name' => '9000']);
