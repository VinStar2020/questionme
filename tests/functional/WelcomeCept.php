<?php 
$I = new FunctionalTester($scenario);

$I->am('a admin');
$I->wantTo('test laravel is working');

//when
$I->amOnPage('/');

//then
$I->seeCurrentUrlEquals('/');
$I->see('Laravel 5', '.title');
