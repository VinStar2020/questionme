<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('create a new question');

// log in as your admin user
// This should be id of 1 if you created your manual login for a known user first.
Auth::loginUsingId(1);
// Add db test data

// add a test user
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'testuser1',
    'email' => 'test1@user.com',
    'password' => 'password',
]);

// Add test categories
$I->haveRecord('surveys', [
    'id' => '9900',
    'title' => 'survey1',
    'detail' => 'survey1 detail',
]);
$I->haveRecord('surveys', [
    'id' => '9901',
    'title' => 'survey2',
    'detail' => 'survey2 detail',
]);


// add a test article to check that content can be seen in list at start

$I->haveRecord('questions', [
    'id' => '9000',
    'question' => 'question 1',
    'questionType' => 'text, required',
    'slug' => 'question1',
    'author_id' => 9999
]);


// add link data for article and category for the test entry
$I->haveRecord('survey_questions', [
    'article_id' => '9000',
    'category_id' => '9900',
]);




// tests /////////////////////////////////////////////

// create an article linked to one category
// When
$I->amOnPage('/admin/questions');
$I->see('Question', 'h1');
$I->see('Question 1');
$I->dontSee('Question 2');
// And
$I->click('Add Question');

// Then
$I->amOnPage('/admin/question/create');
// And
$I->see('Add Question', 'h1');

$I->submitForm('#createquestion', [
    'question' => 'Question 2',
    'questionType' => 'rating, required',
    'slug' => 'question2',
    'category' => '9900',
]);

// how to handle the link table checking.

// check that the article has been written to the db then grab the new id ready to use as input to the link table.
// We don't have to search for the category id as we set that above and so we already know what it should have been set to.
$article = $I->grabRecord('questions', ['title' => 'Question 2']);
$I->seeRecord('answers', ['questions_id' => $article->id, 'surveys_id' => '9900']);

// Then
$I->seeCurrentUrlEquals('/admin/questions');
$I->see('Questions', 'h1');
$I->see('Question 1');
$I->see('Question 2');

$I->click('Question 2'); // the title is a link to the detail page


// Check that the url has a similar path to this.. the last part is a regular expression to allow for a digit or more to be returned as an id.
//$I->seeCurrentUrlMatches('~/admin/articles/(\d+)~');
$I->see('Question 2', 'h1');
$I->see('rating, required');
$I->see('creator: testuser1'); // Got to here in Passing first BBd test 4 errors on line 24
$I->see('surveys:');
$I->see('survey1');