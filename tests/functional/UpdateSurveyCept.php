<?php
$I = new FunctionalTester($scenario);

$I->am('admin');
$I->wantTo('update a Survey');

// log in as your admin user
// This should be id of 1 if you created your manual login for a known user first.
Auth::loginUsingId(1);

// create a category in the db that we can then update
$I->haveRecord('surveys', [
    'id' => '9999',
    'title' => 'Randomtest',
    'detail' => 'a test category',
]);

// Check the user is in the db and can be seen
$I->seeRecord('surveys', ['title' => 'Randomtest', 'id' => '9999']);


// When
$I->amOnPage('/admin/surveys');

// then

// Check  the link is present - this is because there could potentially be many update links/buttons.
// each link can be identified by the users id as name.
$I->seeElement('a', ['name' => '9999']);
// And
$I->click('a', ['name' => '9999']);

// Then
$I->amOnPage('/admin/surveys/9999/edit');
// And
$I->see('Edit Survey - Randomtest', 'h1');

// Then
$I->fillField('title', 'Updatedtest');
// And
$I->click('Update Survey');

// Then
$I->seeCurrentUrlEquals('admin/surveys');
$I->seeRecord('surveys', ['title' => 'Updatedtest']);
$I->see('Surveys', 'h1');
$I->see('Updatedtest');
