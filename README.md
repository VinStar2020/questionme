# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* this repository is for coursework 2 of CIS2301, the website is a questionnaire website that will allow a user to create or answer surveys.
* 0.0.1

### How do I get set up? ###

* laravel 5.2.* will be used alonge side foundation 5
* when setting up laravel the following command should be used to set the correct version {composer create-project laravel/laravel questionme --prefer-dist "5.2.*")
* Dependencies :-

            Node.js
            Bower
            foundation 5.5.2
            codeception 2.1.*
            
* Database configuration
* How to run tests
* Deployment instructions