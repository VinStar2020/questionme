<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Survey;
use App\Question;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        Model::unguard();

        // truncate survey before adding in data with ids that are set.
        User::truncate();
        Survey::truncate();
        Question::truncate();

        Model::reguard();

        //re-enable foreign key check for this connection
        DB::statement('SET FOREIGN_KEY_CHECKS=1;');

        /*
         * Run seed files so known data is created first
         */
        $this->call(UserTableSeeder::class);

        /*
         * run factories
         */
        factory(User::class, 50)->create();
        factory(Survey::class, 5)->create();
        factory(Question::class, 5)->create();
    }
}
