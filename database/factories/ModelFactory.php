<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'remember_token' => str_random(10),
    ];
});
$factory->define(App\Survey::class, function (Faker\Generator $faker) {
    return [
        'title' => $faker->sentence,
        'detail' => $faker->sentence,
        'publishedAt' => $faker->dateTime,
        'author_id' => 1,
    ];
});
$factory->define(App\Question::class, function (Faker\Generator $faker) {
    return [
        'question' => $faker->sentence,
        'questionType' => $faker->word,
        'slug' => $faker->word,
        'show' => $faker->boolean,
    ];
});
