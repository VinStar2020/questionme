@extends('layouts.master')

@section('title', 'Create Survey')

@section('content')
    <title>Create Survey</title>
</head>
<body>
    <h1>Add Survey</h1>

        {!! Form::open(array('action' => 'SurveyController@store', 'id' => 'createsurvey')) !!}
        {{ csrf_field() }}
        <div class="row">
            <div class="large-12 columns">
            {!! Form::label('title', 'Title:',['class' => 'inline large-2 columns']) !!}
            {!! Form::text('title', null,['class' => 'inline large-6 columns']) !!}
            </div>
        </div>

        <div class="row">
            <div class="large-12 columns">
            {!! Form::label('detail', 'Details:',['class' => 'inline large-2 columns']) !!}
            {!! Form::textarea('detail', null,['class' => 'large-12 columns']) !!}
            </div>
        </div>

        <div class="row">
            <div class="large-12 columns">
            {!! Form::label('publishedAt', 'Published At:',['class' => 'inline large-2 columns']) !!}
            {!! Form::text('publishedAt', null,['class' => 'inline large-6 columns', 'placeholder' => 'YYYY-MM-DD HH:MM:SS']) !!}
            </div>
        </div>

        <div class="row">
            {!! Form::submit('Add Survey', ['class' => 'button']) !!}
        </div>
        {!! Form::close() !!}

@endsection