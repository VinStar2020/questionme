@extends('layouts.master')

@section('title', '{{ $survey->title }}')

@section('content')

    <h1>{{ $survey->title }}</h1>
    <h2>{{ $survey->detail }}</h2>
    <h3>{{ $survey->publishedAt }}</h3>

    <section>
        @if (isset ($questions))

            <ul>
                @foreach ($questions as $question)
                    <li>{{ $question->question }}</li>
                    <li>{{ $question->questionType}}</li>
                @endforeach
            </ul>
        @else
            <p> no questions added yet </p>
        @endif
    </section>

    {{ Form::open(array('action' => 'QuestionController@create', 'method' => 'get')) }}
    <div class="row">
        {!! Form::submit('Add Question', ['class' => 'button']) !!}
    </div>
    {{ Form::close() }}
@endsection