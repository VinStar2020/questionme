@extends('layouts.master')

@section('title', 'Add A Question')

@section('content')

    <h1>Add A Question</h1>

    <div class="row">
        {!! Form::open(array('action' => 'QuestionController@store', 'id' => 'createquestion')) !!}
        {{ csrf_field() }}
        <div class="row">
            <div class="large-12 columns">
            {!! Form::label('question', 'Question:',['class' => 'inline large-2 columns']) !!}
            {!! Form::text('question', null,['class' => 'inline large-6 columns']) !!}
            </div>
        </div>

        <div class="row">
            <div class="large-12 columns">
            {!! Form::label('questionType', 'Question Type:',['class' => 'inline large-2 columns']) !!}
            {!! Form::select('questionType', array('Open' => 'open', 'Rating' => 'rating', 'Multi Choice' => 'multiChoice'),['class' => 'inline large-6 columns']) !!}
            </div>
        </div>

        <div class="row">
            <div class="large-12 columns">
            {!! Form::label('slug', 'Slug:',['class' => 'inline large-2 columns']) !!}
            {!! Form::text('slug', null,['class' => 'inline large-6 columns']) !!}
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
            {!! Form::label('survey', 'Survey:',['class' => 'inline large-2 columns']) !!}
            {!! Form::select('survey[]', $survey, null,['class' => 'inline large-6 columns left']) !!}
            </div>
        </div>
        <div class="row">
            <div class="large-12 columns">
            {!! Form::submit('Add Question', ['class' => 'button']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>

@endsection