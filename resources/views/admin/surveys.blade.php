@extends('layouts.master')

@section('title', 'Surveys')

@section('content')
    <h1>Surveys</h1>

    <section>
        @if (isset ($surveys))
            <ul>
                @foreach ($surveys as $survey)
                    <li><a href="/admin/surveys/{{ $survey->id }}" name="{{ $survey->title }}">{{ $survey->title }}</a></li>
                @endforeach
            </ul>
        @else
            <p> no surveys added yet </p>
        @endif
    </section>

    {{ Form::open(array('action' => 'SurveyController@create', 'method' => 'get')) }}
    <div class="row">
        {!! Form::submit('Add Survey', ['class' => 'button']) !!}
    </div>
    {{ Form::close() }}
@endsection