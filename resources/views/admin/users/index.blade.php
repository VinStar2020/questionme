@extends('layouts.master')

@section('title', 'All Users')

@section('content')
            <th>Permissions</th>
                </tr>
                @foreach ($users as $user)
                    <tr>
                        <td><a href="/admin/users/edit/{{ $user->id }}" name="{{ $user->name }}">{{ $user->name }}</a></td>
                        <td> {{ $user->email }}</td>
                        <td>
                            <ul>
                                @foreach($user->roles as $role)
                                    <li>{{ $role->label }}</li>
                                @endforeach
                            </ul>
                        </td>
                    </tr>
                @endforeach
            </table>
        @else
            <p>no users</p>
        @endif
    </section>
@endsection
